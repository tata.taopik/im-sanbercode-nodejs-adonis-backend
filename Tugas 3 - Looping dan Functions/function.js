// Soal No. 1
console.log("Soal NO. 1");

function teriak() {
    return "Halo Sanbers!";
}

console.log(teriak());


//Soal NO. 2
console.log("Soal No. 2");

function kalikan(num1, num2) {
    return num1 * num2;
}

console.log(kalikan(4, 12));


//Soal NO. 3
console.log("Soal No. 3");

function perkenalan(nama, umur, alamat, hobi) {
    return (
        "Nama saya " + 
        nama +
        ", umur saya " + 
        umur +
        " tahun" + ", alamat saya di " + 
        alamat +
        ", dan saya punya hobby yaitu " + 
        hobi +
        "!"
    );
}

console.log(perkenalan("Agus", 30, "Jogja", "Gaming"));

