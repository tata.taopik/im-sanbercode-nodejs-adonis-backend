// Soal No. 1
console.log("Soal NO. 1");

console.log("LOOPING PERTAMA");

i = 2;
while(i <= 20) {
    var output = i + "- I Love Coding";
    console.log(output);
    i += 2;
}

console.log("LOOPING KEDUA");

j=20
while(j >= 2) {
    var output = j + "- I will become a mobile developer";
    console.log(output);
    j -= 2;
}


//Soal No. 2
console.log("Soal No. 2");

for (var k = 1; k <= 20; k++) {
    var output = "";
    if(k % 2 == 1) {
        if (k % 3 == 0) {
            output = k + "- I Love Coding";
        } else {
            output = k + "- Santai";
        }
    } else {
        output = k + "- Berkualitas";
    }
    console.log(output);
}


//Soal No. 3
console.log("Soal No. 3")

function makeRectangle(panjang, lebar) {
    for (var l = 1; l <= lebar; l++) {
        var baris = "";
        for (var p = 1; p <= panjang; p++) {
            baris += "#";
        }
        console.log(baris);
    }
}
makeRectangle(8, 4);


//Soal NO. 4
console.log("Soal No. 4");

function makeLadder(sisi) {
    for (var r = 1; r <= sisi; r++) {
        baris1 = "";
        for (var t = r; t <= sisi; t++) {
            baris1 += "#";
        }
        console.log(baris1);   
    }
}

makeLadder(7);
