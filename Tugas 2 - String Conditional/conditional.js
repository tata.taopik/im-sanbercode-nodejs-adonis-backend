// if-else
console.log("Soal Conditional if-else")


// Output untuk Input nama = '' dan peran = ''
"Nama harus diisi!"

var nama = "";
var peran = "";

if (nama == "" && peran == "") {
    console.log("Nama harus diisi!");
} else if (nama = "John") {
    if (peran == ""){
        console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
    }
}
 
//Output untuk Input nama = 'John' dan peran = ''
"Halo John, Pilih peranmu untuk memulai game!"
 
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
"Selamat datang di Dunia Werewolf, Jane"
"Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"

var nama = "Jane";
var peran = "Penyihir";

if (nama != "" && peran != "") {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Halo " + peran + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
}
 
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
"Selamat datang di Dunia Werewolf, Jenita"
"Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
 
//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
"Selamat datang di Dunia Werewolf, Junaedi"
"Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" 


//switch-case
console.log("Soal Conditional Switch-Case")

var hari = 21; 
var bulan = 5; 
var tahun = 1945;

switch (bulan) {
    case 1:
        console.log(hari + " Januari " + tahun);
        break;
    case 2:
        console.log(hari + " Februari " + tahun);
        break;
    case 3:
        console.log(hari + " Maret " + tahun);
        break;
    case 4:
        console.log(hari + " April " + tahun);
        break;
    case 5:
        console.log(hari + " Mei " + tahun);
        break;
    case 6:
        console.log(hari + " Juni " + tahun);
        break;
    case 7:
        console.log(hari + " Juli " + tahun);
        break;    
    case 8:
        console.log(hari + " Agustus " + tahun);
        break;
    case 9:
        console.log(hari + " September " + tahun);
        break;
    case 10:
        console.log(hari + " Oktober " + tahun);
        break;
    case 11:
        console.log(hari + " November " + tahun);
        break;
    case 12:
        console.log(hari + " Desember " + tahun);
        break;
    default:
        console.log("Bulan tidak valid");
        break; 
}