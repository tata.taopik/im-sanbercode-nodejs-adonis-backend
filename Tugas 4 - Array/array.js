// Soal No. 1
console.log("Soal no. 1");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(input) {
    for (var i = 0; i < input.length; i++) {
        console.log("Nomor ID: " , input[i][0])
        console.log("Nama Lengkap: " , input[i][1])
        console.log("TTL: " , input[i][2] + " " + input[i][3])
        console.log("Hobi: " , input[i][4])
        console.log("");
    } 
}

dataHandling(input);

//Soal No. 2
console.log("Soal No. 2");
function balikKata(string) {
    var output = ""
    var panjangString = string.length
     for (var a = panjangString - 1; a >= 0; a--) {
        output += string[a]
     }
     
     return output
}

console.log(balikKata("SanberCode"))
//Output: edoCrebnaS

console.log(balikKata("racecar")) 
//Output: racecar

console.log(balikKata("kasur rusak"))
//Output: kasur rusak

console.log(balikKata("haji ijah"))
//Output: haji ijah

console.log(balikKata("I am Sanbers"))
//Output: srebnaS ma I